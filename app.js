let express = require('express');
let path = require('path');

let converterRouter = require('./Routes/ConverterRoute');

let app = express();

let bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({'extended':'true'}));
app.use(bodyParser.json());

let port = process.env.PORT || 3090;

let router = express.Router();

router.get('*', (req, res) => res.sendFile(path.join(__dirname, 'public', 'index.html')));

// aplica as rotas na aplicação
app.use('/', router);
app.use('/api',converterRouter);

app.listen(port, () => console.log('Servidor escutando na porta '+port));