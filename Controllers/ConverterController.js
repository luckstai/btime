let _ = require('lodash');

let conversao = ((req,res) => {

    let nomes = req.body;
    let palavras = [];
    let padronizados = [];
    let proibidos = ['filho', 'filha', 'neto', 'neta', 'sobrinho', 'sobrinha', 'junior'];
    let removidos=[];
    let results= [];

    _.forEach(nomes, function(chave, valor) {

        palavras.push(chave.toLowerCase().split(/\s+/));
    });

    _.forEach(palavras, function(chave, valor) {

        let posicaoUltimoNome = chave.length-1;

        let concatenado = chave.concat(proibidos);

        if(verificarDuplicidade(concatenado)){

            if(posicaoUltimoNome >= 2){

                removidos.push(palavras[valor].splice(posicaoUltimoNome-1, 2));
            }
            else{

                removidos.push(palavras[valor].splice(posicaoUltimoNome, 1));
            }
        }
        else{

            removidos.push(palavras[valor].splice(posicaoUltimoNome, 1));
        }

        padronizados.push(chave);
    });

    formatarStrings(padronizados);

    _.forEach(removidos,function(chave, valor){

        let virgula = '';

        if(padronizados[valor] != ''){
            virgula = ', ';
        }

        results.push((chave.join(' ').toUpperCase()+ virgula +padronizados[valor]));
    });

    res.json({results});
});

function ucfirst(str){

    return str.substr(0,1).toUpperCase()+str.substr(1)
}

function verificarDuplicidade(array) {

    return (new Set(array)).size !== array.length;
}

function formatarStrings(array){

    _.forEach(array,function(chave,valor){

        _.forEach(chave,function(indice,item){

            if(item === 0){

                array[valor][item] = indice.toUpperCase();
            }

            if(indice.length > 3 & item !== 0){

                array[valor][item] = ucfirst(indice);
            }
        });

        array[valor] = chave.join(' ');
    });

    return array;
}

module.exports =  {conversao};