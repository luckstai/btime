const express = require('express');
const converterRouter = express.Router();

let converterController = require('../Controllers/ConverterController');

converterRouter.route('/converter').post(converterController.conversao);

module.exports = converterRouter;